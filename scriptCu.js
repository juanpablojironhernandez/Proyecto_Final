$(document).ready(function() {
 $.ajax({
            url: 'http://localhost:8080/cursos',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('tbody');

                tbodyEl.html('');

                response.forEach(function(curso) {
                    tbodyEl.append('\
                        <tr>\
                             <td class="id">' + curso.idCurso + '</td>\
                            <td><input type="text" class="name" value="' + curso.nombre + '"></td>\
                            <td><input type="text" class="teacher" value="' + curso.profesor + '"></td>\
                            <td><input type="text" class="date" value="' + curso.fecha +  '"></td>\
                            <td>\
                                <button class="update-button">UPDATE/PUT</button>\
                                <button class="delete-button">DELETE</button>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });



$(function() {
    // READ/GET
    $('#get-button').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/cursos',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('tbody');

                tbodyEl.html('');

                response.forEach(function(curso) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + curso.idCurso + '</td>\
                            <td><input type="text" class="name" value="' + curso.nombre + '"></td>\
                            <td><input type="text" class="teacher" value="' + curso.profesor + '"></td>\
                            <td><input type="text" class="date" value="' + curso.fecha +  '"></td>\
                            <td>\
                                <button class="update-button">UPDATE</button>\
                                <button class="delete-button">DELETE</button>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-form').on('submit', function(event) {
        event.preventDefault();

        var info = { 
			nombre: $('#nombre').val(),
			profesor : $('#profesor').val(),
			fecha: $('#fecha').val(),
	
		};

        $.ajax({
            url: 'http://localhost:8080/curso/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#nombre').val(''),
				$('#profesor').val(''),
				$('#fecha').val(''),
                $('#get-button').click();
            }
        });
    });

    // UPDATE/PUT
    $('table').on('click', '.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();
        var newNombre = rowEl.find('.name').val();
        var newProfesor = rowEl.find('.teacher').val();
        var newFecha = rowEl.find('.date').val();


        var info = { 
            nombre: newNombre,
            profesor: newProfesor,
            fecha: newFecha,
        };
        $.ajax({
            url: 'http://localhost:8080/curso/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });

    // DELETE
    $('table').on('click', '.delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();

        $.ajax({
            url: 'http://localhost:8080/curso/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });
});