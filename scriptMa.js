$(document).ready(function() {
    $.ajax({
        url: 'http://localhost:8080/matriculas',
        contentType: 'application/json',
        success: function(response) {
            var tbodyEl = $('tbody');

            tbodyEl.html('');

            response.forEach(function(matricula) {
                tbodyEl.append('\
                    <tr>\
                        <td class="id">' + matricula.idMatricula + '</td>\
                        <td><input type="text" class="idcursos" value="' + matricula.curso.nombre + '"></td>\
                        <td><input type="text" class="idpersonas" value="' + matricula.persona.nombre + '"></td>\
                        <td><input type="text" class="fechamatriculas" value="' + matricula.fechamatricula + '"></td>\
                        <td>\
                            <button class="update-button">UPDATE/PUT</button>\
                            <button class="delete-button">DELETE</button>\
                        </td>\
                    </tr>\
                ');
            });
        }
    });
});

$(function() {
    // READ/GET
    $('#get-button').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/matriculas',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('tbody');

                tbodyEl.html('');

                response.forEach(function(matricula) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + matricula.idMatricula + '</td>\
                            <td><input type="text" class="idcursos" value="' + matricula.curso.nombre + '"></td>\
                            <td><input type="text" class="idpersonas" value="' + matricula.persona.nombre + '"></td>\
                            <td><input type="text" class="fechamatriculas" value="' + matricula.fechamatricula + '"></td>\
                            <td>\
                                <button class="update-button">UPDATE</button>\
                                <button class="delete-button">DELETE</button>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-form').on('submit', function(event) {
        event.preventDefault();

        var id_c = $("#idcurso").val();
        var id_p = $("#idpersona").val();

        var persona = {
            idPersona: id_p
        }

        var curso = {
            idCurso: id_c
        }

        var info = { 
            fechamatricula: $('#fechamatricula').val(),
            persona: persona,
            curso: curso	
		};

        $.ajax({
            url: 'http://localhost:8080/matricula/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
				$('#idcurso').val(''),
				$('#idpersona').val(''),
                $('#fechamatricula').val(''),
                $('#get-button').click();
            }
        });
    });

    // UPDATE/PUT
    $('table').on('click', '.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();
        var newFecham = rowEl.find('.fechamatricula').val();
        var newIdc = rowEl.find('.idcurso').val();
        var newIdp = rowEl.find('.idpersona').val();


        var info = { 
            fechamatricula: newFecham,
            idcurso: newIdc,
            idpersona: newIdp,  
        };
        $.ajax({
            url: 'http://localhost:8080/matricula/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });

    // DELETE
    $('table').on('click', '.delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();

        $.ajax({
            url: 'http://localhost:8080/matricula/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });
});