$(document).ready(function() {
$.ajax({
            url: 'http://localhost:8080/personas',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('tbody');

                tbodyEl.html('');

                response.forEach(function(persona) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + persona.idPersona + '</td>\
                            <td><input type="text" class="name" value="' + persona.nombre + '"></td>\
                            <td><input type="text" class="lastname" value="' + persona.apellido + '"></td>\
                            <td><input type="int" class="age" value="' + persona.edad + '"></td>\
                            <td>\
                                <button class="update-button">UPDATE/PUT</button>\
                                <button class="delete-button">DELETE</button>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });



$(function() {
    // READ/GET
    $('#get-button').on('click', function() {
        $.ajax({
            url: 'http://localhost:8080/personas',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('tbody');

                tbodyEl.html('');

                response.forEach(function(persona) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + persona.idPersona + '</td>\
                            <td><input type="text" class="name" value="' + persona.nombre + '"></td>\
                            <td><input type="text" class="lastname" value="' + persona.apellido + '"></td>\
                            <td><input type="int" class="age" value="' + persona.edad + '"></td>\
                            <td>\
                                <button class="update-button">UPDATE</button>\
                                <button class="delete-button">DELETE</button>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-form').on('submit', function(event) {
        event.preventDefault();

        var info = { 
			nombre: $('#nombre').val(),
			apellido: $('#apellido').val(),
			edad: $('#edad').val(),	
		};

        $.ajax({
            url: 'http://localhost:8080/persona/',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#nombre').val(''),
				$('#apellido').val(''),
				$('#edad').val(''),
                $('#get-button').click();
            }
        });
    });

    // UPDATE/PUT
    $('table').on('click', '.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();
        var newName = rowEl.find('.name').val();
        var newLastName = rowEl.find('.lastname').val();
        var newEdad = rowEl.find('.age').val();


        var info = { 
            nombre: newName,
            apellido: newLastName,
            edad: newEdad, 
        };
        $.ajax({
            url: 'http://localhost:8080/persona/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(info),
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });

    // DELETE
    $('table').on('click', '.delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();

        $.ajax({
            url: 'http://localhost:8080/persona/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });
});